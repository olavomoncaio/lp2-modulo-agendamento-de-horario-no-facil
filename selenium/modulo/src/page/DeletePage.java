package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DeletePage extends BasePage {

	public DeletePage(WebDriver driver, int agendamento_id) {
		super(driver, "deletar/" + agendamento_id);
	}
	
	public TarefaPage removeTarefa(int agendamento_id) {
		driver.findElement(By.className("delete-btn")).click(); 
		return new TarefaPage(driver, agendamento_id);
	}

}
