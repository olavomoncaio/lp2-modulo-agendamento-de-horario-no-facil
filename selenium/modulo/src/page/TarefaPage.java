package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TarefaPage extends BasePage {

	public TarefaPage(WebDriver driver, int pessoa_id) {
		super(driver, "agenda/criar/" + pessoa_id);
	}

	public boolean temNomeTurma(String nome) {
		return driver.getPageSource().contains(nome);
	}

	public boolean temTarefas() {
		return driver.getPageSource().contains("atendente");
	}
	
	public boolean formularioOculto() {
		WebElement element = driver.findElement(By.id("novo_agendamento"));
		return ! hasClass(element, "show");
	}
	
	public boolean formularioExibido() {
		WebElement button = driver.findElement(By.id("collapsebutton"));
		button.click();
		
		ExpectedCondition<Boolean> condition = ExpectedConditions.attributeContains(By.id("novo_agendamento"), "class", "show");
		return new WebDriverWait(driver, 10).until(condition);
	}
	
	public boolean criaTarefas() {
		if(formularioExibido()) {
			WebElement atendente = driver.findElement(By.name("atendente"));
			WebElement data = driver.findElement(By.name("data"));
			WebElement hora = driver.findElement(By.name("hora"));
			WebElement servico = driver.findElement(By.name("servico"));
			WebElement telefone = driver.findElement(By.name("telefone"));
			
			atendente.sendKeys("Olavo Moncaio Grilenzoni");
			data.sendKeys("2019-09-23");
			hora.sendKeys("20:00");
			servico.sendKeys("O uso de testes E2E � fundamental para a entrega de software de alta qualidade");
			telefone.sendKeys("1159878548");

			
			WebElement submit = driver.findElement(By.className("btnupload-form"));
			submit.click();
			
			return driver.getPageSource().contains("2019-06-06") && 
			driver.getPageSource().contains("Teste automatizado com Selenium");
		}
		return false;
	}

	public EditPage editaTarefas(int agendamento_id) {
		criaTarefas();
		driver.findElement(By.id("editar_1")).click();
		return new EditPage(driver, agendamento_id);
	}

	public boolean comfirmaEdicao() {
		return driver.getPageSource().contains("2019-06-07") && 
		driver.getPageSource().contains("Agendamento editado com sucesso");
	}

	public DeletePage removeTarefa(int agendamento_id) {
		criaTarefas();
		driver.findElement(By.id("deletar_1")).click();
		return new DeletePage(driver, agendamento_id);
	}

	public boolean comfirmaRemocao() {
		return ! temTarefas();
	}
}
