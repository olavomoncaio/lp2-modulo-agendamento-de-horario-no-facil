package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EditPage extends BasePage {

	public EditPage(WebDriver driver, int agendamento_id) {
		super(driver, "editar/" + agendamento_id);
	}

	public TarefaPage alteraDadosTarefa(int pessoa_id) {
		WebElement atendente = driver.findElement(By.name("atendente"));
		WebElement telefone = driver.findElement(By.name("telefone"));
		WebElement servico = driver.findElement(By.name("servico"));
		WebElement data = driver.findElement(By.name("data"));
		WebElement hora = driver.findElement(By.name("hora"));
		
		atendente.clear();
		telefone.clear();
		servico.clear();
		data.clear();
		hora.clear();
		atendente.sendKeys("Roberval Gomes");		
		telefone.sendKeys("1123569987");		
		servico.sendKeys("Emiss�o de novo passaporte com visto internacional Europeu");		
		data.sendKeys("2019-10-28");
		hora.sendKeys("12:30");
		driver.findElement(By.className("btnupload-form")).click();
		
		return new TarefaPage(driver, pessoa_id);
	}

}
