package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import page.DeletePage;
import page.EditPage;
import page.TarefaPage;

public class TarefaTest extends BaseTest {

	@Test
	public void criaTarefa() {
		TarefaPage tarefaPage = new TarefaPage(driver, 2);
		boolean b = tarefaPage.criaTarefas();
		assertTrue("Os agendamentos n�o foram criados", b);
	}
	
	@Test
	public void editaTarefa() {
		int agendamento_id = 1;
		int pessoa_id = 2;
		
		TarefaPage tarefaPage = new TarefaPage(driver, pessoa_id);
		EditPage editPage = tarefaPage.editaTarefas(agendamento_id);
		tarefaPage = editPage.alteraDadosTarefa(pessoa_id);
		boolean b = tarefaPage.comfirmaEdicao();
		assertTrue("Erro na edi��o dos agendamentos", b);	
	}
	
	@Test
	public void removeTarefa() {
		int agendamento_id = 1;
		int pessoa_id = 3;
		
		TarefaPage tarefaPage = new TarefaPage(driver, pessoa_id);
		DeletePage deletePage = tarefaPage.removeTarefa(agendamento_id);
		tarefaPage = deletePage.removeTarefa(pessoa_id);
		boolean b = tarefaPage.comfirmaRemocao();
		assertTrue("Erro na remo��o dos agendamentos", b);	
	}
	
}
