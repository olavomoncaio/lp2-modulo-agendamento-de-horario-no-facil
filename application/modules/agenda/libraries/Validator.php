<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Validator extends CI_Object{

    public function form_agenda(){
        $this->form_validation->set_rules('atendente', 'atendente', 'trim|required|min_length[1]|max_length[128]');
        $this->form_validation->set_rules('data', 'data', 'trim|required|exact_length[10]');
        $this->form_validation->set_rules('hora', 'hora', 'trim|required|min_length[1]|max_length[128]');
        $this->form_validation->set_rules('servico', 'servico', 'trim|required|min_length[1]|max_length[128]');
        $this->form_validation->set_rules('telefone', 'telefone', 'trim|required|min_length[1]|max_length[128]');
        return $this->form_validation->run();
    }
}