<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Agendamentos extends Dao {

    function __construct(){
        parent::__construct('agenda_horarios');
    }

    public function insert($data, $table = null) {
        $cols = array('atendente', 'data', 'hora', 'servico', 'telefone', 'pessoa_id');
        $this->expected_cols($cols);
        return parent::insert($data);
    }
}