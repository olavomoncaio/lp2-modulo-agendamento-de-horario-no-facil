<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/Dao.php';

class Cadastros extends Dao{

    function __construct(){
        parent::__construct('cadastros');
    }
    
    public function lista(){ 
        return array(
            array('id' => 1, 'Olavo Moncaio', 'Adulto', '(11) 2641-5121', '105.987.884-51'),
            array('id' => 2, 'João Martinês', 'Adulto', '(11) 7621-5513', '987.854.222-69'),
            array('id' => 3, 'Antônio da Silva', 'Adulto', '(11) 2316-2312', '231.242.212-69'),
            array('id' => 4, 'Pedro Gonçalves', 'Criança', '(11) 1231-5456', '321.224.298-51'),
            array('id' => 5, 'Valéria Cristina', 'Adulto', '(11) 5531-2312', '512.112.298-12'),
            array('id' => 6, 'Joaquim Penha', 'Criança', '(11) 5123-2312', '134.224.334-21'),
            array('id' => 7, 'Illana Martins', 'Adulto', '(11) 4112-5661', '096.224.754-88'),
        );
    }
        
    public function nome($pessoa_id){
        $v = array('', 'Olavo Moncaio', 'João Martinês', 'Antônio da Silva', 'Pedro Gonçalves', 'Valéria Cristina', 'Joaquim Penha', 'Illana Martins');
        return $v[$pessoa_id];
    }
}
