<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/component/buttons/EditDeleteButtonGroup.php';
include_once APPPATH.'libraries/component/Table.php';

class AgendaModel extends CI_Model{

    public function fake_list(){
        $this->load->library('Cadastros');
        $data = $this->cadastros->lista();
        $header = array('#', 'Nome', 'Tipo', 'Telefone', 'CPF');
        $table = new Table($data, $header);
        $table->action('agenda/criar');
        $table->zebra_table();
        $table->use_border();
        $table->use_hover();
        return $table->getHTML();
    }

    public function lista_agendamentos($pessoa_id){
        $header = array('#', 'Atendente', 'Data', 'Hora', 'Serviço', 'Telefone');
        $this->load->library('Agendamentos', null, 'agendamentos');
        $this->agendamentos->cols(array('id', 'atendente', 'data', 'hora', 'servico', 'telefone'));
        $data = $this->agendamentos->get(array('pessoa_id' => $pessoa_id));
        if(! sizeof($data)) return '';
        
        $table = new Table($data, $header);
        $table->zebra_table();
        $table->use_border();
        $table->use_hover();
        
        $edbg = new EditDeleteButtonGroup('agenda');
        $table->use_action_button($edbg);
        return $table->getHTML();
    }

    public function novo_agendamento($pessoa_id){
        if(! sizeof($_POST)) return;
        $this->load->library('Validator', null, 'valida');

        if($this->valida->form_agenda()){
            $this->load->library('Agendamentos', null, 'agendamentos');
            $data = $this->input->post();
            $data['pessoa_id'] = $pessoa_id;
            $this->agendamentos->insert($data);
        }
        else return true;
    }

    public function edita_agendamento($agendamento_id){
        $this->load->library('Agendamentos', null, 'agendamentos');
        $this->load->library('Validator', null, 'valida');
        $task = $this->agendamentos->get(array('id' => $agendamento_id));

        if(sizeof($_POST) && $this->valida->form_agenda()){
            $data = $this->input->post();
            $data['id'] = $agendamento_id;
            $id = $this->agendamentos->insert_or_update($data);
            if($id) redirect('agenda/criar/'.$task[0]['pessoa_id']);
        }
        else {
            foreach ($task[0] as $key => $value)
                $_POST[$key] = $value;
            return $_POST['pessoa_id'];
        }
    }

    public function deleta_agendamento($agendamento_id) {
        $this->load->library('Agendamentos', null, 'agendamentos');
        $task = $this->agendamentos->get(array('id' => $agendamento_id));
        
        if(sizeof($_POST)) {
            if($this->agendamentos->delete(array('id' => $agendamento_id)))
                redirect('agenda/criar/'.$task[0]['pessoa_id']);
        }
        else return $task[0];
    }

    public function nome_usuario($pessoa_id){
        $this->load->library('Cadastros');
        return $this->cadastros->nome($pessoa_id);
    }
}