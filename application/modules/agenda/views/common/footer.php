<footer class="page-footer font-small teal pt-4">

  <div class="container-fluid text-center text-md-left">

    <div class="row">

      <div class="col-md-6 mt-md-0 mt-3">

        <h5 class="text-uppercase font-weight-bold">Agendamento Fácil</h5>
        <p>Aqui você pode realizar agendamentos para diversos serviços de maneira simples e rápida, através de um administrador
        do sistema. Este funcionário poderá também editar e apagar agendamentos, de acordo com a necessidade.</p>

      </div>

      <hr class="clearfix w-100 d-md-none pb-3">

      <div class="col-md-6 mb-md-0 mb-3">

        <h5 class="text-uppercase font-weight-bold">Segurança e Controle</h5>
        <p>Os agendamentos ficam salvos em nossa base de dados, assim além de garantior maior segurança e confiabilidade
        de dados, é possível fazer a controladoria de todos os agendamentos.</p>

      </div>

    </div>
  

  </div>

  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="https://mdbootstrap.com/education/bootstrap/"> Olavo Moncaio Grilenzoni - Gu3002446</a>
  </div>


</footer>
