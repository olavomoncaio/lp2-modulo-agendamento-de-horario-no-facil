<div class="container mt-3 <?= $show_form ? '' : 'collapse' ?>" id="novo_agendamento">
    <div class="card">
        <div class="card-header"><h4>Novo agendamento</h4></div>
        <div class="card-body">
            <form method="POST" class="text-center border border-light p-4" id="task-form">
                <div class="form-row mb-4">
                    <div class="col-md-6">
                        <input type="text" name="atendente" value="<?= set_value('atendente') ?>" class="form-control" placeholder="Atendente">
                    </div>
                    <div class="col-md-3">
                        <input type="date" name="data" value="<?= set_value('data') ?>" class="form-control" placeholder="Data de Agendamento" maxlength="8">
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="hora" value="<?= set_value('hora') ?>" class="form-control" placeholder="Horário">
                    </div>
                </div>
                <div class="form-row mb-4">
                    <div class="col-md-9">
                        <input type="text" name="servico" value="<?= set_value('servico') ?>" class="form-control" placeholder="Serviço Desejado">
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="telefone" value="<?= set_value('telefone') ?>" class="form-control" placeholder="Telefone">
                    </div>
                </div>
                <div class="text-center text-md-right">
                    <a class="btnupload-form btn btn-primary" onclick="document.getElementById('task-form').submit();">Agendar</a>
                </div>
            </form>
        </div>
    </div>
</div>