<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Agenda extends MY_Controller{

    public function __construct(){
        $this->load->model('AgendaModel', 'model');
    }

    public function index(){
        $data['titulo'] = 'Lista de Cadastros';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['lista'] = $this->model->fake_list();
        $html = $this->load->view('main2', $data, true);
        $this->show($html);
        $this->load->view('common/footer');
    }

    public function criar($pessoa_id){
        $this->add_script('tarefa/mascara');
        $this->validate_id($pessoa_id);     
        $data['show_form'] = $this->model->novo_agendamento($pessoa_id);
        $data['home'] = true;
        $nome = $this->model->nome_usuario($pessoa_id);
        $data['titulo'] = "Agendamentos no Fácil - $nome";
        $data['rotulo_botao'] = 'Novo Agendamento';
        $data['form_subject'] = 'novo_agendamento';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('form_agenda', $data, true);
        $data['lista'] = $this->model->lista_agendamentos($pessoa_id);
        $html = $this->load->view('main', $data, true);
        $this->show($html);
        $this->load->view('common/footer');
    }
    
    public function editar($agendamento_id){
        $this->validate_id($agendamento_id);
        $pessoa_id = $this->model->edita_agendamento($agendamento_id);
        $nome = $this->model->nome_usuario($pessoa_id);
        $data['show_form'] = true;
        $data['titulo'] = "Editar Agendamento de $nome";
        $data['rotulo_botao'] = 'Novo Agendamento';
        $data['form_subject'] = 'nova_agendamento';
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $data['formulario'] = $this->load->view('agenda/form_agenda', $data, true);
        $html = $this->load->view('main', $data, true);
        $this->show($html);
        $this->load->view('common/footer');
    }

    public function deletar($agendamento_id){
        $this->validate_id($agendamento_id);
        $pessoa_id = $this->model->edita_agendamento($agendamento_id);
        $data = $this->model->deleta_agendamento($agendamento_id);
        $nome = $this->model->nome_usuario($pessoa_id);
        $data['home'] = true;
        $data['titulo'] = "Remover Agendamento de $nome";
        $data['turma'] = $this->model->nome_usuario($data['pessoa_id']);
        $data['topo_pagina'] = $this->load->view('topo_pagina', $data, true);
        $html = $this->load->view('confirm_delete', $data, true);
        $this->show($html);
    }

}