<?php
include_once APPPATH.'controllers/test/builder/TestDataBuilder.php';

class AgendamentosDataBuilder extends TestDataBuilder {

    public function __construct($table = 'lp2_modulo'){
        parent::__construct('agenda_horarios', $table);
    }

    function getData($index = -1){
        $data[0]['data'] = '2019-06-05';
        $data[0]['atendente'] = 'Olavo Moncaio';
        $data[0]['pessoa_id'] = 10;
        $data[0]['servico'] = 'Implementação de dados';
        $data[0]['telefone'] = '1133226548';
        $data[0]['hora'] = '15:00';
 

        $data[1]['data'] = '2019-06-04';
        $data[1]['atendente'] = 'Valéria Costa';
        $data[1]['pessoa_id'] = 6;
        $data[1]['servico'] = 'Comprovação de renda';
        $data[0]['telefone'] = '1126548549';


        $data[2]['data'] = '2019-06-03';
        $data[2]['atendente'] = 'Joaquim Ribeiro';
        $data[2]['pessoa_id'] = 3;
        $data[2]['servico'] = 'Emissão de recursos';
        $data[2]['telefone'] = '1152699287';

        return $index > -1 ? $data[$index] : $data;
    }

}