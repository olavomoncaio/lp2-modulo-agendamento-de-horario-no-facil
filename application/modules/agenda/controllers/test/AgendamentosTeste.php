<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/agenda/libraries/Agendamentos.php';
include_once APPPATH . 'modules/agenda/controllers/test/builder/AgendamentosDataBuilder.php';

class AgendamentosTeste extends Toast{
    private $builder;
    private $agendamento;

    function __construct(){
        parent::__construct('AgendamentosTeste');
    }

    function _pre(){
        $this->builder = new AgendamentosDataBuilder();
        $this->agendamento = new Agendamentos();
    }

    // apenas ilustrativo
    function test_objetos_criados_corretamente(){
        $this->_assert_true($this->builder, "Erro na criação do builder");
        $this->_assert_true($this->agendamento, "Erro na criação da agendamento");
    }

    // apenas ilustrativo
    function test_selecionado_banco_de_teste(){
        $s = $this->builder->database();
        $this->_assert_equals('lp2_modulo', $s, 'Erro na seleção do banco de teste');
    }

    function test_insere_registro_na_tabela(){
        // cenário 1: vetor com dados corretos
        $this->builder->clean_table();
        
        $data = $this->builder->getData(0);
        $id1 = $this->agendamento->insert($data, 'agenda_horarios');
        $this->_assert_equals(1, $id1, "Esperado 1, recebido $id1");

        // verificação: o objeto criado é, de fato, aquele que enviamos?

        $task = $this->agendamento->get(array('id' => 1))[0];
        $this->_assert_equals($data['atendente'], $task['atendente']);
        $this->_assert_equals($data['data'], $task['data']);
        $this->_assert_equals($data['servico'], $task['servico']);
        $this->_assert_equals($data['telefone'], $task['telefone']);
        $this->_assert_equals($data['hora'], $task['hora']);


        // cenário 2: vetor vazio
        $id2 = $this->agendamento->insert(array());
        $this->_assert_equals(-1, $id2, "Esperado -1, recebido $id2");

        // cenário 3: vetor com dados inesperados
        $info = $this->builder->getData(1);
        $info['unexpected_col_name'] = 1;
        $id = $this->agendamento->insert($info);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");

        // cenário 4: vetor com dados incompletos... deve ser
        // tratado pela validação, mas tem que ser pensado aqui
        $v = array('atendente' => 'vetor incompleto');
        $id = $this->agendamento->insert($v);
        $this->_assert_equals(-1, $id, "Esperado -1, recebido $id");
    }

    function test_carrega_todos_os_registros_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        $tasks = $this->agendamento->get();
        $this->_assert_equals(3, sizeof($tasks), "Número de registros incorreto");
    }

    function test_carrega_registro_condicionalmente(){
        $this->builder->clean_table();
        $this->builder->build();

        $task = $this->agendamento->get(array('data' => '2019-06-04', 'pessoa_id' => 6))[0];
        $this->_assert_equals('2019-06-04', $task['data'], "Erro na data");
        $this->_assert_equals(6, $task['pessoa_id'], "Erro no id da pessoa");
    }
    
    function test_atualiza_registro(){
        $this->builder->clean_table();
        $this->builder->build();

        // lê um registro do bd
        $task1 = $this->agendamento->get(array('id' => 2))[0];
        $this->_assert_equals('2019-06-04', $task1['data'], "Erro na data");
        $this->_assert_equals(6, $task1['pessoa_id'], "Erro no id da pessoa");

        // atualiza seus valores
        $task1['data'] = '2019-06-02';
        $task1['pessoa_id'] = 8;
        $this->agendamento->insert_or_update($task1);

        // lê novamente, o mesmo objeto, e verifica se foi atualizado
        $task2 = $this->agendamento->get(array('id' => 2))[0];
        $this->_assert_equals($task1['data'], $task2['data'], "Erro na data");
        $this->_assert_equals($task1['pessoa_id'], $task2['pessoa_id'], "Erro no id da pessoa");
    }
    
    function test_remove_registro_da_tabela(){
        $this->builder->clean_table();
        $this->builder->build();

        // verifica que o registro existe
        $task1 = $this->agendamento->get(array('id' => 2))[0];
        $this->_assert_equals('2019-06-04', $task1['data'], "Erro na data");
        $this->_assert_equals(6, $task1['pessoa_id'], "Erro no id da pessoa");

        // remove o registro
        $this->agendamento->delete(array('id' => 2));

        // verifica que o registro não existe mais
        $task2 = $this->agendamento->get(array('id' => 2));
        $this->_assert_equals_strict(0, sizeof($task2), "Registro não foi removido");
    }

}