<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/agenda/libraries/Agendamentos.php';
include_once APPPATH . 'modules/agenda/controllers/test/builder/AgendamentosDataBuilder.php';

class E2ETest extends Toast{

    function __construct(){
        parent::__construct('E2E Test');
    }

    function test_limpa_tabela_de_teste(){
        $builder = new AgendamentosDataBuilder('lp2_modulo_test');
        $builder->clean_table();

        $agendamentos = new Agendamentos();
        $data = $agendamentos->get();
        $this->_assert_equals_strict(0, sizeof($data), 'Erro na limpeza da tabela de teste');
    }

}