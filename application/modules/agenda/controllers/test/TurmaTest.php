<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH . '/controllers/test/Toast.php';
include_once APPPATH . 'modules/agenda/libraries/Cadastros.php';

class PessoaTest extends Toast{
    private $pessoa;

    function __construct(){
        parent::__construct('PessoaTest');
    }

    function _pre(){
        $this->pessoa = new Pessoa();
    }

    function test_carrega_lista_de_pessoas(){
        $v = $this->pessoa->lista();
        $this->_assert_equals(4, sizeof($v), "Número de pessoas incorreto");
    }

    function test_gera_nome_das_pessoas(){
        $nome = $this->pessoa->nome(2);
        $this->_assert_equals('5º Ano B', $nome, "Erro no nome da pessoa");
    }

}