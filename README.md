## AT03 - Agendamento de Horário no Fácil

Este repositório contém grande parte das configurações antes estabelecidas pelo professor, além de conter a inclusão do módulo agenda, que refere-se ao tema Agendamento de Horário no Fácil.

####Quais são as funcionalidades do código?
Tem como funciolidade as funções padrões de um agendamento de diversos serviços para pessoas cadastradas no sistema, possibilitando também um controle sobre esses agendamentos, permitindo a exclusão e edição de dados, além da inserção.

------------


# PHP

####Como iniciar o sistema? 
1. Instale o Xampp, localizado em **https://www.apachefriends.org/pt_br/download.html**.
2. Instale o Firefox caso ainda não o possua em sua máquina **https://www.mozilla.org/pt-BR/firefox/new/**.
3. Crie uma pasta chamada modulo na pasta htdocs, localizada no diretório de instalação do Xampp.
4. Clone o projeto para a pasta módulo.
5. Abra o **localhost/phpmyadmin**, crie um banco de dados chamado lp2_modulo e importe o arquivo agendas_horarios.sql localizado na pasta sql, em modulo.
6. Abra o Xampp e dê start na opções de Apache e MySQL.
7. Digite em seu navegador (preferência Firefox) o link **localhost/modulo/agenda**.

####Como inserir novos agendamentos?
1. Entre em **localhost/modulo/agenda**.
2. Será listada uma lista (fake) de usuários, clique no que terá um novo agendamento.
3. Na nova página que irá abrir, serão listados todos os agendamentos do usuário selecionado (caso existam agendamentos).
4. Clique em Novo Agendamento e preencha os campos solicitados.
5. Clique em Agendar.
6. O novo agendamento estará listado na página.

####Como remover agendamentos?
1. Entre em **localhost/modulo/agenda**.
2. Será listada uma lista (fake) de usuários, clique no que terá um agendamento removido.
3. Na nova página que irá abrir, serão listados todos os agendamentos do usuário selecionado (caso existam agendamentos).
4. Clique na lixeira localizada no canto direito do agendamento que será removido.

####Como editar agendamentos?
1. Entre em **localhost/modulo/agenda**.
2. Será listada uma lista (fake) de usuários, clique no que terá um novo agendamento.
3. Na nova página que irá abrir, serão listados todos os agendamentos do usuário selecionado (caso existam agendamentos).
4. Clique no ícone de edição do agendamento desejado.
6. Você será redirecionado ao formulário de agendamento, altere os dados desejados e clique em Agendar.

####Testes no PHP
1. Entre em **http://localhost/modulo/agenda/test/all**
2. Todos os testes serão realizados e será possível saber o que está dando certo ou não.
3. **Todos os dados serão limpos quando o teste for realizado!**

------------


# Java

####Configurar ambiente para testes de aceitação
1. Copie a pasta **selenium** para C:/ do Windows
2. Abra o Eclipse e crie um projeto chamado **modulo**
3. Clique com o botão direito sobre o projeto
4. Escolha a última opção: **Properties**
5. À esquerda clique em **Java Build Path** e selecione a aba **Libraries**
6. À direita clique no botão **Add Library** e selecione o JUnit 4
7. Clique no segundo botão **Add External Jars...** e navegue até à pasta **selenium/libs**.  Selecione todos os arquivos .jar e clique em abrir.
8. Clique no botão **Ok** para concluir esta etapa de configuração.

#### Executar testes de aceitação do módulo tarefa
1. Importe os arquivos de teste para o Eclipse clicando no menu File -> Import -> General -> File System -> Browse. Navegue até à pasta **selenium/test** e clique na pasta **src**.
2. Na janela que se abre marque o *checkbox* ao lado da pasta **src** e clique em **finish**.
3. Abra e execute o arquivo RegressionTest.java, que se encontra na pasta *default package*.

#### Testes no Java
1. O Projeto em java está localizado na pasta **modulo/selenium/modulo**.
2. Abra o Projeto usando o Eclipse e execute-o.
3. O programa fará inserções, remoções, e edições de dados.
------------

# Informações

####Dados do aluno
Nome: Olavo Moncaio Grilenzoni
Prontuário: Gu3002446
3º Semestre
Linguagem de Programação II (LP II)
Instituto Federal de Educação, Ciência e Tecnologia - Campus Guarulhos
Data: 22/06/2019


------------
